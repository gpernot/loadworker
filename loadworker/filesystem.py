import gzip
import os
import random
import shutil
from importlib.resources import files

from fastapi import Request
from pydantic import BaseModel

from base import BaseTest
from dbench import DispositionEnum, OptionsEnum, StatusEnum

_operations = []

random_buffer = random.randbytes(64 * 1024)


class FilesystemTestStart(BaseModel):
    workdir: str


class FilesystemTestRun(BaseModel):
    batch: int = 1


class FilesystemTest(BaseTest):
    @staticmethod
    async def startup():
        # load filesystem operations

        with gzip.open(
            files("loadworker.data").joinpath("filesystem.ops.gz"), "rt"
        ) as f:
            current_set = []
            for line in f.readlines():
                line = line.strip()
                if not line:
                    # flush current command set
                    if current_set:
                        _operations.append(current_set)
                        current_set = []
                    continue
                if line[0] == "#":
                    # comment
                    continue
                try:
                    command, args = line.split(" ", 1)
                except ValueError:
                    command, args = line, ""
                current_set.append((command, args.split()))
            if current_set:
                _operations.append(current_set)
        print(f"Loaded {len(_operations)} test requests")

    async def start(self, request: Request, workdir: str):
        request.session["req_index"] = 0
        request.session["sessiondir"] = os.path.join(
            os.path.expanduser(workdir), request.session.get("session_id")
        )

    async def stop(self, request: Request):
        assert request.session["sessiondir"]
        shutil.rmtree(request.session["sessiondir"], ignore_errors=True)

    async def run(self, request: Request, batch: int):
        req_index = request.session.get("req_index") % len(_operations)
        self._sessiondir = request.session.get("sessiondir")
        print(f"req_index {req_index}")

        for i in range(req_index, min(req_index + batch, len(_operations))):
            for command, args in _operations[i]:
                method = getattr(self, "do_" + command)
                await method(*[cast(a) for cast, a in zip(method.types, args)])

        request.session["req_index"] = min(req_index + batch, len(_operations))

    async def do_close(self):
        self._handle.close()

    do_close.types = ()

    async def do_deltree(self, dirname):
        shutil.rmtree(os.path.join(self._sessiondir, dirname), ignore_errors=True)

    do_deltree.types = (str,)

    async def do_list_dir(self, dirname, maxcount):
        for f in os.listdir(os.path.join(self._sessiondir, dirname)):
            os.lstat(os.path.join(self._sessiondir, dirname, f))

    do_list_dir.types = (str, int)

    async def do_flush(self):
        self._handle.flush()

    do_flush.types = ()

    async def do_lock(self, offset, size):
        self._handle.seek(offset, os.SEEK_SET)
        os.lockf(self._handle.fileno(), os.F_LOCK, size)

    do_lock.types = (int, int)

    async def do_mkdir(self, dirname: str):
        os.makedirs(os.path.join(self._sessiondir, dirname), exist_ok=True)

    do_mkdir.types = (str,)

    async def do_open(self, filename, options, disposition, status):
        op = DispositionEnum[disposition]
        if op == DispositionEnum.FILE_CREATE:
            mode = "w+b"
        elif op == DispositionEnum.FILE_OPEN:
            mode = "r+b"
        elif op == DispositionEnum.FILE_OVERWRITE_IF:
            mode = "wb"
        else:
            raise Exception(f"Unknown disposition {disposition}")
        try:
            self._handle = open(os.path.join(self._sessiondir, filename), mode)
        except FileNotFoundError:
            if status != StatusEnum.NT_STATUS_OBJECT_NAME_NOT_FOUND:
                raise

    do_open.types = (str, int, str, str)

    async def do_query_file(self, level):
        pass

    do_query_file.types = (int,)

    async def do_query_fs(self, level):
        pass

    do_query_fs.types = (int,)

    async def do_query_path(self, filename, level, status):
        pass

    do_query_path.types = (str, int, str)

    async def do_read(self, offset, size):
        self._handle.seek(offset, os.SEEK_SET)
        self._handle.read(size)

    do_read.types = (int, int)

    async def do_rename(self, old, new):
        shutil.move(
            os.path.join(self._sessiondir, old), os.path.join(self._sessiondir, new)
        )

    do_rename.types = (str, str)

    async def do_set_file_information(self, level):
        pass

    do_set_file_information.types = (int,)

    async def do_unlink(self, filename):
        os.remove(os.path.join(self._sessiondir, filename))

    do_unlink.types = (str,)

    async def do_unlock(self, offset, size):
        self._handle.seek(offset, os.SEEK_SET)
        os.lockf(self._handle.fileno(), os.F_ULOCK, size)

    do_unlock.types = (int, int)

    async def do_write(self, offset, size):
        self._handle.seek(offset, os.SEEK_SET)
        if size <= len(random_buffer):
            self._handle.write(random_buffer[:size])
        else:
            self._handle.write(random.randbytes(size))

    do_write.types = (int, int)
