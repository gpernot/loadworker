# Load test worker #

## Dev ##

### Running ###

from `loadworker/`:

```
uvicorn server:app --reload
```

### Testing ###


```
curl http://localhost:8000/filesystem -X POST -H "x-auth-token: foo" -H "Content-Type: application/json" -d '{"workdir": "/tmp", "timelimit": "30"}'
```


from `tests/`:

```
locust -H http://localhost:8000 --users 10 --spawn-rate 1 --headless
```
