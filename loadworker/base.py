from pydantic import BaseModel


class BaseTest:
    """Base class for all test"""

    @staticmethod
    async def startup():
        """Lifespan startup"""

    @staticmethod
    async def shutdown():
        """Lifespan shutdown"""

    async def start(self, request):
        """Initialize test"""

    async def stop(self, request):
        """Teardown"""

    async def run(self, request):
        """Run test"""
