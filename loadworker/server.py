import time
import uuid
from contextlib import asynccontextmanager
from typing import Annotated

from fastapi import Cookie, FastAPI, Request, Response
from fastapi.responses import JSONResponse
from starlette.middleware.sessions import SessionMiddleware

from filesystem import FilesystemTest, FilesystemTestRun, FilesystemTestStart

SECRET_KEY = "foobar"

sessions = []


@asynccontextmanager
async def lifespan(app: FastAPI):
    await FilesystemTest.startup()
    yield
    await FilesystemTest.shutdown()


app = FastAPI(lifespan=lifespan)


@app.middleware("http")
async def authorized(request: Request, call_next):
    if request.headers.get("x-auth-token") != "foo":
        return JSONResponse(
            status_code=401,
            content={"message": "Invalid shared secret"},
        )
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["call-duration"] = str(process_time)
    return response


@app.middleware("http")
async def autosession(request: Request, call_next):
    if request.session.get("session_id") is None:
        session_id = str(uuid.uuid4())
        request.session["session_id"] = session_id
        sessions.append(session_id)

    response = await call_next(request)
    return response


app.add_middleware(SessionMiddleware, secret_key=SECRET_KEY)


@app.get("/")
async def root():
    return {"foo": "bar"}


@app.get("/ping")
async def ping():
    return {"response": "Ok"}


@app.post("/filesystem/start")
async def filesystem_start(
    request: Request,
    params: FilesystemTestStart,
):
    await FilesystemTest().start(request, params.workdir)
    return {"response": "Ok"}


@app.post("/filesystem/stop")
async def filesystem_stop(
    request: Request,
):
    await FilesystemTest().stop(request)
    return {"response": "Ok"}


@app.post("/filesystem/run")
async def filesystem_run(request: Request, params: FilesystemTestRun):
    await FilesystemTest().run(request, params.batch)
    return {"response": "Ok"}
