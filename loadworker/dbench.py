# definitions used by dbench for filesystem tests
from enum import IntEnum, StrEnum


class DispositionEnum(IntEnum):
    FILE_SUPERSEDE = 0
    FILE_OPEN = 1
    FILE_CREATE = 2
    FILE_OPEN_IF = 3
    FILE_OVERWRITE = 4
    FILE_OVERWRITE_IF = 5

    @classmethod
    def from_str(cls, value):
        return cls(int(value, 16))


class OptionsEnum(IntEnum):
    FILE_NONE = 0x0000
    FILE_DIRECTORY_FILE = 0x0001
    FILE_WRITE_THROUGH = 0x0002
    FILE_SEQUENTIAL_ONLY = 0x0004
    FILE_NON_DIRECTORY_FILE = 0x0040
    FILE_NO_EA_KNOWLEDGE = 0x0200
    FILE_EIGHT_DOT_THREE_ONLY = 0x0400
    FILE_RANDOM_ACCESS = 0x0800
    FILE_DELETE_ON_CLOSE = 0x1000

    @classmethod
    def from_str(cls, value):
        return cls(int(value, 16))


class StatusEnum(StrEnum):
    NT_STATUS_OK = "NT_STATUS_OK"
    NT_STATUS_OBJECT_NAME_NOT_FOUND = "NT_STATUS_OBJECT_NAME_NOT_FOUND"
    NT_STATUS_OBJECT_PATH_NOT_FOUND = "NT_STATUS_OBJECT_PATH_NOT_FOUND"
    NT_STATUS_NO_SUCH_FILE = "NT_STATUS_NO_SUCH_FILE"
