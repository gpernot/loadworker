import time

from locust import HttpUser, task
from locust.exception import InterruptTaskSet


class FilesystemUser(HttpUser):
    @task
    def filesystem_batch50(self):
        headers = {"x-auth-token": "foo"}
        data = {"batch": 50}
        response = self.client.post("/filesystem/run", headers=headers, json=data)
        if response.status_code != 200:
            raise InterruptTaskSet

    def on_start(self):
        headers = {"x-auth-token": "foo"}
        data = {"workdir": "~/tmp/locust"}
        self.client.post("/filesystem/start", headers=headers, json=data)

    def on_stop(self):
        headers = {"x-auth-token": "foo"}
        self.client.post("/filesystem/stop", headers=headers)
